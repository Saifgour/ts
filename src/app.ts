import express from 'express';
import path from 'path'
import http from 'http'
const app = express();
const port = 4000;

app.use(express.static(path.join(__dirname, '../public/build')));

// app.use('/', (req, res) => {
//   res.send('saif');
// });
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});
const server = http.createServer(app)
server.listen(port)
server.on('error', (err: any) => {
  if (err) {
    return console.error(err);
  }
}); 
server.on('listening',  () => {
       console.error(`server is listening on port : ${port}`);
  
});
